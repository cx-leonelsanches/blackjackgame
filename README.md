# Blackjack Game in C#

This repository is used for many purposes, from CxFlow to CxQL demonstrations.

This is a Blackjack game implemented using C# and .NET Framework 4.7.2, but it can be compiled in .NET 6. 

## Running

You can use either Visual Studio or command line.

### Compiling using the command line

At project root:

    $ csc ./BlackjackGame/*.cs

This will generate a .exe file. 